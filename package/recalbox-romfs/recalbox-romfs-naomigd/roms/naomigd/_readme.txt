## RECALBOX - SYSTEM SEGA NAOMI GD ##

Put your naomigd roms in this directory.

Rom files must have a ".lst/.dat/.zip/.7z" extension.
Find the DAT file in the folder "/recalbox/share/bios/dc/"
.chd files need a .zip file to be started.

This system allows to use compressed roms on .zip/.7z.
But, it is only an archive. Files inside the .zip/.7z must match with extensions mentioned above.
Each .zip/.7z file must contain only one compressed rom.
